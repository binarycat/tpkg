use std::fs::File;
use std::io::{Seek, SeekFrom};
use std::env;
use std::process::Command;
use tempfile;
use zip;

static TRAILER_OFFSET: &str = "__TPKG_OFFSET_MAGIC_00000000000000000000_END_MAGIC__";

// TODO: signal handler that cleans up the created temporary dir, although this only matters if tpkg itself receives the signal

fn main() {
	let offset = TRAILER_OFFSET[20..40].parse::<u64>().unwrap();
	let binpath = env::current_exe().unwrap();
	let mut bin = File::open(binpath).unwrap();
	bin.seek(SeekFrom::Start(offset)).unwrap();
	let dir = tempfile::Builder::new().prefix("tpkg").tempdir().unwrap();
	zip::ZipArchive::new(bin).unwrap().extract(dir.path()).unwrap();
	let _status = Command::new(dir.path().join("initialize"))
		.env("TPKG_APPDIR", dir.path())
		.env("PATH", format!("{}/bin:{}", dir.path().display(),
							 env::var("PATH").unwrap()))
		.status().unwrap();
	// prevent dir from going out of scope before we're done with it
	_ = dir;
}
