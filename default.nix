{ pkgs ? import <nixpkgs> {}
/*, stdenv ? pkgs.pkgsMusl.stdenv
, cargo ? pkgs.pkgsMusl.cargo
, rustc ? pkgs.pkgsMusl.rustc */
}:
let
  inherit (pkgs.pkgsMusl) rustc cargo stdenv;
  inherit (pkgs) lua5_4;
in
stdenv.mkDerivation {
  name = "tpkg";
  buildInputs = [ rustc cargo lua5_4 ];
  src = ./.;
  env.RUSTFLAGS = "-C target-feature=+crt-static";
  installPhase = "install -DT target/release/tpkg $out/bin/tpkg";
}
