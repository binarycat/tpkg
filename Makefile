profile = release
tpkg = target/$(profile)/tpkg

$(tpkg): src/main.rs Cargo.toml
	cargo build --profile $(profile)
	strip $@
	lua patch.lua $@

clean:
	rm -r target

examples: target/example/env

target/example:
	mkdir $@

target/example/%.zip: example/% target/example
	cd $<; zip out.zip *
	mv $</out.zip $@

target/example/%: $(tpkg) target/example/%.zip
	cat $^ > $@
	chmod +x $@



