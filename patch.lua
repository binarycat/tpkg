-- patch executible so it knows its own length

local filename = ...
assert(filename, "filename not specified")
local file, err = io.open(filename, "r+")
if err then error(err) end
local exe = file:read("a")

local len = #exe

local new = string.format("__TPKG_OFFSET_MAGIC_%020d_END_MAGIC__", len)

local pos = string.find(exe, "__TPKG_OFFSET_MAGIC_00000000000000000000_END_MAGIC__")

assert(pos, "magic string not found")

file:seek("set", pos-1)

file:write(new)
file:close()

