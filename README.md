trivial package bundler, like AppImages, but a lot simpler

# build dependancies
* cargo
* make
* lua 5.3

# usage

## package your application into a zip file
this will be extracted into a temporary directory (stored in $TPKG_APPDIR) at program startup. (tpkg automatically closes this after your program exits)

all binary dependancies should be in `bin/`, as this will be prepended to PATH.

after $PATH is set, tpkg will run the executable named `initialize` at the root of the zip file.  note that this file must be marked as executible, otherwise this will not work.

`initialize` is reccomended to be a shell script that does additional setup before calling your application, such as prepending `$TPKG_APPDIR/lib` to `LD_LIBRARY_PATH`

## build the tpkg binary
run `make`

## append the zip file to the binary
```
cat target/release/tpkg app.zip > myapp
chmod +x myapp
```

